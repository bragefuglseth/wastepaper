/* main.js
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Adw from 'gi://Adw?version=1';

import { WastepaperWindow } from './window.js';

pkg.initGettext();
pkg.initFormat();

export const WastepaperApplication = GObject.registerClass({
    GTypeName: 'WastepaperApplication',
}, class WastepaperApplication extends Adw.Application {
    constructor() {
        super({
            application_id: 'com.feaneron.Wastepaper',
            flags: Gio.ApplicationFlags.DEFAULT_FLAGS,
        });

        this.add_action_entries([
            {
                name: 'about',
                activate: () => {
                    const aboutDialog = new Adw.AboutDialog({
                        application_name: 'Wastepaper',
                        application_icon: 'com.feaneron.Wastepaper',
                        developer_name: 'Georges Stavracas',
                        version: '0.1.0',
                        developers: [
                            'Georges Stavracas'
                        ],
                        artists: [
                            'Brage Fuglseth https://bragefuglseth.dev'
                        ],
                        copyright: '© 2023 Georges Stavracas'
                    });
                    aboutDialog.present(this.active_window);
                },
            },
            {
                name: 'activate',
                activate: () => this.activate(),
            },
            {
                name: 'quit',
                activate: () => this.quit(),
            },
        ]);

        this.set_accels_for_action('app.activate', ['<primary>n']);
        this.set_accels_for_action('app.quit', ['<primary>q']);
    }

    vfunc_startup() {
        super.vfunc_startup();

        const styleManager = Adw.StyleManager.get_default();
        styleManager.color_scheme = Adw.ColorScheme.FORCE_LIGHT;
    }

    vfunc_activate() {
        const window = new WastepaperWindow(this);
        window.present();
    }
});

export function main(argv) {
    const application = new WastepaperApplication();
    return application.runAsync(argv);
}

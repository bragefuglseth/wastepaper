/*
 * taskRow.js
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Adw from 'gi://Adw';
import GObject from 'gi://GObject';

import { WastepaperTask } from './task.js';

export const WastepaperTaskRow = GObject.registerClass({
    GTypeName: 'WastepaperTaskRow',
    Template: 'resource:///com/feaneron/Wastepaper/taskRow.ui',
    Properties: {
        'task': GObject.ParamSpec.object(
            'task', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            WastepaperTask),
    },
    InternalChildren: ['titleText'],
}, class WastepaperTaskRow extends Adw.PreferencesRow {
    constructor(task) {
        super({task});

        this.task.bind_property('title',
            this._titleText, 'text',
            GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
    }
});


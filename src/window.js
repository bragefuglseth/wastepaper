/* window.js
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Adw from 'gi://Adw';

import { WastepaperSentinelListModel } from './model.js';
import { WastepaperNewTaskRow } from './newTaskRow.js';
import { WastepaperTask } from './task.js';
import { WastepaperTaskRow } from './taskRow.js';

export const WastepaperWindow = GObject.registerClass({
    GTypeName: 'WastepaperWindow',
    Template: 'resource:///com/feaneron/Wastepaper/window.ui',
    InternalChildren: [
        'listbox',
    ],
}, class WastepaperWindow extends Adw.ApplicationWindow {
    constructor(application) {
        super({ application });

        this._tasksModel = new Gio.ListStore(WastepaperTask);

        this._model = new WastepaperSentinelListModel({model: this._tasksModel});
        this._listbox.bind_model(this._model, item => this._createRow(item));
    }

    _createRow(item) {
        let row = null;

        if (item instanceof WastepaperTask) {
            row = new WastepaperTaskRow(item);
        } else {
            row = new WastepaperNewTaskRow();
            row.connect('task-created', (_, task) => this._tasksModel.append(task));
        }

        console.assert(row !== null);

        return row;
    }

    _addTask(title) {
        console.assert(title?.length > 0);
        this._tasksModel.append(new WastepaperTask({title}));
    }
});

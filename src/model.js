/*
 * model.js
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

export const WastepaperSentinel = GObject.registerClass({
    GTypeName: 'WastepaperSentinel',
}, class WastepaperSentinel extends GObject.Object {
});

export const WastepaperSentinelListModel = GObject.registerClass({
    GTypeName: 'WastepaperSentinelListModel',
    Implements: [Gio.ListModel],
    Properties: {
        'has-sentinel': GObject.ParamSpec.boolean('has-sentinel', '', '',
            GObject.ParamFlags.READWRITE,
            true),
        'model': GObject.ParamSpec.object('model', '', '',
            GObject.ParamFlags.READWRITE,
            Gio.ListModel.$gtype),
    },
}, class WastepaperSentinelListModel extends GObject.Object {
    constructor(params = {}) {
        super(params);

        this._hasSentinel = true;
        this._sentinel = new WastepaperSentinel();
    }

    vfunc_get_item_type() {
        return GObject.Object.$gtype;
    }

    vfunc_get_n_items() {
        const nItems = this._model ? this._model.nItems : 0;
        return this._hasSentinel ? nItems + 1 : nItems;
    }

    vfunc_get_item(index) {
        const nItems = this.get_n_items();

        if (index >= nItems)
            return null;

        return index === nItems - 1 ? this._sentinel : this._model?.get_item(index);
    }

    get has_sentinel() {
        return this._hasSentinel;
    }

    set has_sentinel(v) {
        if (this._hasSentinel === v)
            return;

        this._hasSentinel = v;

        if (this._hasSentinel)
            this.items_changed(this.get_n_items(), 0, 1);
        else
            this.items_changed(this.get_n_items(), 1, 0);

        this.notify('has-sentinel');
    }

    get model() {
        return this._model;
    }

    set model(v) {
        if (this._model === v)
            return;

        let nAdded = 0;
        let nRemoved = 0;

        if (this._model) {
            nRemoved = this._model.get_n_items();
            this._model.disconnect(this._itemsChangedId);
            delete this._itemsChangedId;
        }

        this._model = v;

        if (this._model) {
            nAdded = this._model.get_n_items();
            this._itemsChangedId = this._model.connect(
                'items-changed',
                (_, position, removed, added) => {
                    this.items_changed(position, removed, added);
                });
        }

        this.notify('model');
        this.items_changed(0, nRemoved, nAdded);
    }
});
